# Multi-Step Form - README

## Overview

### The challenge

Users should be able to:

- Complete each step of the sequence
- Go back to a previous step to update their selections
- See a summary of their selections on the final step and confirm their order
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- Receive form validation messages if:
  - A field has been missed
  - The email address is not formatted correctly
  - A step is submitted, but no selection has been made

## Video Demo

[![Alt Text](https://i9.ytimg.com/vi_webp/tlprSmBhAbo/mq1.webp?sqp=CJjh0aMG&rs=AOn4CLAlSP0tKXF9qpN6MQaxH4mXwAB5UA)](https://www.youtube.com/watch?v=tlprSmBhAbo)


Click the image above to watch a video demonstration of the multi-step form in action.


### Links

- Live Site URL:[](https://quiet-salmiakki-e1f1f1.netlify.app/)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- JavaScript
- Event listeners and DOM manipulation
- Mobile-first workflow

### What I learned

During the development of this project, I gained insights into several key areas:

- Creating a multi-step form using JavaScript and DOM manipulation.
- Implementing form validation to ensure all required fields are filled correctly.
- Dynamically generating HTML elements based on data using JavaScript.
- Updating and calculating prices based on user selections.
- Handling radio buttons and checkboxes for plan and add-on selections.
- Toggling between monthly and yearly billing options.
- Navigating between form steps and displaying the active step.
- Enhancing the user experience with interactive elements and styles.

Here's an example of a code snippet I'm proud of:

```javascript
function handleNextButtonClick() {
  if (currentStep === 0 && !validateForm()) {
    return;
  }
  if (currentStep === 1 && !document.querySelector(".select-plan.active")) {
    alert("Please select a plan.");
    return;
  }
  // Rest of the code...
}
```

This function handles the "Next Step" button click event and performs form validation and plan selection checks before proceeding to the next step.


### Useful Resources

During the development of this project, I found the following resources helpful:

- [MDN Web Docs](https://developer.mozilla.org/) - A comprehensive web development resource with detailed documentation on HTML, CSS, JavaScript, and more.
- [W3Schools](https://www.w3schools.com/) - An online learning platform with tutorials and references for web development technologies.
- [CSS-Tricks](https://css-tricks.com/) - A website that provides helpful tips, tricks, and examples for CSS and front-end development.
