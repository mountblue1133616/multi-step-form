document.addEventListener("DOMContentLoaded", function () {
  let isYearlyBilling = false;
  const contents = document.querySelectorAll(".contents");
  const nextBtn = document.querySelector(".next-btn button");
  const backBtn = document.querySelector(".back-btn");
  let currentStep = 0;
  const sidebarButtons = document.querySelectorAll(".sidebar-btn button");
  const addonsParentElement = document.querySelector(".add-ons-singles");
  const toggleSwitch = document.getElementById("toggle");
  const parentaddon = document.querySelector(".add-on-parent");
  const singleplanparent = document.querySelector(".single-plan-parent");
  const planContainers = singleplanparent.childNodes;
  const addonCheckboxes = [];
  const selectedAddons = [];
  let selectedPlan = {
    name: "",
    amount: null,
    year: false,
  };
  // Data for "Pick-Addons" and "Select-Plan"
  const planData = [
    {
      name: "Arcade",
      amount: 9,
      image: "./public/media/images/icon-arcade.svg",
    },
    {
      name: "Advanced",
      amount: 12,
      image: "./public/media/images/icon-advanced.svg",
    },
    {
      name: "Pro",
      amount: 15,
      image: "./public/media/images/icon-pro.svg",
    },
  ];

  const addonData = [
    {
      label: "Online Service",
      description: "Helps add up to 10 online players",
      price: 1,
    },
    {
      label: "Larger Storage",
      description: "Extra space for 1TB",
      price: 2,
    },
    {
      label: "Customizable Profile",
      description: "Custom theme on your profile",
      price: 2,
    },
  ];

  // DOM FOR PAGE 2
  planData.forEach((plan, index) => {
    const planContainer = document.createElement("div");
    planContainer.className = "select-plan";

    const radioInput = document.createElement("input");
    radioInput.type = "radio";
    radioInput.name = "plan";
    radioInput.id = `plan-${index}`;
    radioInput.value = plan.name;
    const label = document.createElement("label");
    label.htmlFor = `plan-${index}`;
    label.className = "select-plan-label";
    const imageDiv = `
      <div class="select-plan-img">
        <img src=${plan.image} alt="icon"/>
      </div>
    `;
    const feeTypeDiv = `
      <div class="select-plan-fee-type">
        <h2 class="name-plan">${plan.name}</h2>
        <span>$</span>
        <span data-price=${plan.amount} class="price">${plan.amount} </span>
        <span class="month">${isYearlyBilling ? "/yr" : "/mo"}</span>
      </div>
      <h2 class="free">${isYearlyBilling ? "2 months free" : ""}</h2>
    `;
    label.innerHTML = imageDiv + feeTypeDiv;
    planContainer.append(radioInput, label);
    singleplanparent.appendChild(planContainer);
  });

  // DOM FOR PAGE 3
  addonData.forEach((addon, index) => {
    const addonContainer = document.createElement("div");
    addonContainer.className = "addon-container";
    const checkboxDiv = `<div class="addon-checkbox">
    <input type="checkbox" name="addon" value="${addon.label}">
    </div>
    `;
    const contentDiv = `
    <div class="addon-content">
  <h1 class="addon-label">${addon.label}</h1>
  <div class="addon-description">${addon.description}</div>
  </div>
    `;
    const priceDiv = `
  <div class="addon-price">
  <span>+</span>
  <span>$</span>
  <span class="price" data-price="${addon.price}">${addon.price}</span>
  <span class="month">${isYearlyBilling ? "/yr" : "/mo"}</span>
  </div>
    `;
    addonContainer.innerHTML = checkboxDiv + contentDiv + priceDiv;
    const checkboxInput = addonContainer.querySelector("input");
    addonCheckboxes.push(checkboxInput);
    parentaddon.appendChild(addonContainer);
  });

  // FUNCTIONS
  function showStep(stepIndex) {
    contents.forEach((content, index) => {
      content.classList.toggle("none", index !== stepIndex);
    });
    backBtn.style.display = stepIndex === 0 ? "none" : "inline-block";
    nextBtn.textContent =
      stepIndex === contents.length - 1 ? "Confirm" : "Next Step";
    backBtn.style.display =
      stepIndex === contents.length - 1 ? "none" : "inline-block";

    sidebarButtons.forEach((button, index) => {
      button.classList.toggle("active-btn", index === stepIndex);
    });
  }
  function validateForm() {
    const ERRORS = {
      name: "Name should only contain alphabets without spaces or numbers.",
      email: "Invalid email address. Please enter a valid email.",
      "ph-number":
        "Phone number should be 10 digits long and contain only digits.",
    };
    const activeContent = document.querySelector(".contents:not(.none)");
    const inputs = activeContent.querySelectorAll("input");
    let isValid = true;
    inputs.forEach((input) => {
      const inputName = input.name;
      const inputValue = input.value.trim();
      const errorMessage = input.nextElementSibling;

      if (inputValue.length === 0) {
        isValid = false;
        errorMessage.textContent = `Please enter ${inputName}.`;
        errorMessage.classList.add("error");
        return;
      }
      if (!isGeneralValid(inputValue, inputName)) {
        isValid = false;
        errorMessage.textContent = ERRORS[inputName];
        errorMessage.classList.add("error");
        return;
      }
      errorMessage.textContent = "";
      errorMessage.classList.remove("error");
    });
    return isValid;
  }

  function isGeneralValid(value, inputName) {
    switch (inputName) {
      case "name":
        return isValidName(value);
      case "email":
        return isValidEmail(value);
      case "ph-number":
        return isValidPhoneNumber(value);
      default:
        return true;
    }
  }
  function isValidName(name) {
    return /^[A-Za-z\s]*$/.test(name) && !/\d/.test(name);
  }
  function isValidEmail(email) {
    const atIndex = email.indexOf("@");
    const dotIndex = email.lastIndexOf(".");
    return atIndex > 0 && dotIndex > atIndex + 1 && dotIndex < email.length - 1;
  }
  function isValidPhoneNumber(phoneNumber) {
    return /^\d{10}$/.test(phoneNumber);
  }
  function updateTotalPrice() {
    const total = document.querySelector(".total-price .price");
    const addonPrice = document.querySelectorAll(".add-on-price .price");
    const planNameElement = document.querySelector(".plan-name");
    const planPriceElement = document.querySelector(".plan-price .price");
    let totalPrice = Number(selectedPlan.amount);

    addonsParentElement.innerHTML = "";
    selectedAddons.forEach((addon) => {
      const addOnSingleDiv = `
      <div class="add-on-single">
      <div class="add-on-name">${addon.addon_name}</div>
      <div class="add-on-price">
      <span>$</span>
      <span class="price">${
        addon.addon_price * (isYearlyBilling ? 10 : 1)
      }</span>
      <span class="month">${isYearlyBilling ? "/yr" : "/mo"}</span>
      </div>
      </div>`;
      addonsParentElement.innerHTML += addOnSingleDiv;
      totalPrice += Number(addon.addon_price);
    });
    planNameElement.textContent = selectedPlan.name;

    planPriceElement.textContent =
      selectedPlan.amount * (isYearlyBilling ? 10 : 1);
    total.textContent = totalPrice * (isYearlyBilling ? 10 : 1);
    addonPrice.forEach((priceElement) => {
      const addonPriceValue = parseFloat(priceElement.textContent.trim());
      if (!isNaN(addonPriceValue)) {
        priceElement.textContent = (
          addonPriceValue * (isYearlyBilling ? 10 : 1)
        ).toString();
      } else {
        priceElement.textContent = "0";
      }
    });
  }
  function updatePrices() {
    const monthSpans = document.querySelectorAll(".month");
    const priceElements = document.querySelectorAll(".price");
    const multiplier = isYearlyBilling ? 10 : 1;
    priceElements.forEach((priceElement) => {
      const currentPrice = Number(priceElement.dataset.price);
      priceElement.textContent = currentPrice * multiplier;
    });
    addonCheckboxes.forEach((checkbox, index) => {
      const addonContainer = checkbox.closest(".addon-container");
      const priceElement = addonContainer.querySelector(".addon-price .price");
      const currentPrice = Number(priceElement.textContent.trim());
      priceElement.textContent = currentPrice * multiplier;
    });

    monthSpans.forEach((monthSpan) => {
      monthSpan.textContent = isYearlyBilling ? "/yr" : "/mo";
    });

    updateTotalPrice();
  }
  function handlePlanSelection() {
    planContainers.forEach((planContainer, index) => {
      const radioInput = planContainer.querySelector("input[type='radio']");
      radioInput.addEventListener("change", () => {
        planContainers.forEach((container) => {
          container.classList.remove("active");
        });
        if (radioInput.checked) {
          planContainer.classList.add("active");
          selectedPlan = { ...planData[index] };
          selectedPlan.year = isYearlyBilling;
          updatePrices();
          updateTotalPrice();
        }
      });
    });
  }

  function handleAddonSelection() {
    addonCheckboxes.forEach((checkbox, index) => {
      const addonValue = checkbox.value;
      const priceElement = checkbox
        .closest(".addon-container")
        .querySelector(".price");
      const price = addonData[index].price;
      if (checkbox.checked) {
        const selectedAddon = {
          addon_name: addonValue,
          addon_price: price,
        };
        selectedAddons.push(selectedAddon);
        priceElement.textContent = isYearlyBilling ? price * 10 : price;
      }
      checkbox.onchange = () => {
        if (checkbox.checked) {
          const selectedAddon = {
            addon_name: addonValue,
            addon_price: price,
          };
          selectedAddons.push(selectedAddon);
        } else {
          const addonIndex = selectedAddons.findIndex(
            (addon) => addon.addon_name === addonValue
          );
          if (addonIndex !== -1) {
            selectedAddons.splice(addonIndex, 1);
          }
        }
        updatePrices();
        updateTotalPrice();
      };
    });
  }
  function handleToggleSwitch() {
    isYearlyBilling = toggleSwitch.checked;
    updatePrices();
    planContainers.forEach((planContainer, index) => {
      const priceElement = planContainer.querySelector(".price");
      const price = planData[index].amount;
      priceElement.textContent = isYearlyBilling ? price * 10 : price;
    });
  }
  function handleNextButtonClick() {
    if (currentStep === 0 && !validateForm()) {
      return;
    }
    if (currentStep === 1 && !document.querySelector(".select-plan.active")) {
      alert("Please select a plan.");
      return;
    }
    currentStep++;
    if (currentStep < contents.length) {
      showStep(currentStep);
    } else if (currentStep === contents.length) {
      document.getElementById("myForm").submit();
    }
  }
  function handleBackButtonClick() {
    if (currentStep > 0) {
      currentStep--;
      showStep(currentStep);
    }
  }
  showStep(currentStep);
  handlePlanSelection();
  handleAddonSelection();
  toggleSwitch.addEventListener("change", handleToggleSwitch);
  nextBtn.addEventListener("click", handleNextButtonClick);
  backBtn.addEventListener("click", handleBackButtonClick);
});
